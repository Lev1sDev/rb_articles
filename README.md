# Каталог статей для RB.RU

## Стек технологий: Python 3, Django 3.2, SQLite, Unittest, Faker (наполнение базы данных случайными пользователями и статьями), Beautiful Soup (извлечь данные со страниц статей сайта RB.RU)

## Используется пагинация статей, реализованы Management command для заполнения базы случайными значениями и для заполнения каталога статей с сайта RB.RU. Для авторов в admin django grappelli добавлен визуальный инструмент сортировки по номеру позиции (недюсь, что в ТЗ подразумевался именно он). Написаны unit-тесты, проверяющие работу сервиса

## Инструкция по запуску приложения:

### Склонировать проект:
``` git clone https://gitlab.com/Lev1sDev/rb_articles.git  ```

### Создать виртуальное окружение:
``` python3 -m venv venv```\
``` source venv/bin/activate ```

### Установить зависимости:
``` pip3 install -r requirements.txt ```

### Выполнить миграции:
``` python3 manage.py migrate ```

### Запустить тесты сервиса:
``` python3 manage.py test ```

### Выполнить Management Command для заполнения каталога случайными значениями:
``` python3 manage.py loaddata_authors ```\
``` python3 manage.py loaddata_articles ```

### Выполнить Management Command для заполнения каталога заметками с сайта RB.RU (загрузка идет 2 минуты):
``` python3 manage.py loaddata_articles_from_rb ```

### Создать суперпользователя:
``` python3 manage.py createsuperuser ```

### Запустить сервер:
``` python3 manage.py runserver ```

### Если не подгрузилась статика:
``` python3 manage.py collectstatic```
