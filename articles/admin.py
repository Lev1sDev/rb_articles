from django.contrib import admin

from .models import Article, Author


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ('pk', 'title', 'text',)
    ordering = ('-pk',)


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    list_display = ('pk', 'first_name', 'last_name', 'position')
    sortable_field_name = 'position'
