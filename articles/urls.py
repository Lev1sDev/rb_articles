from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('new/', views.new_article, name='new_article'),
    path('authors/', views.author_index, name='author_index'),
    path('authors/<int:author_id>/', views.profile, name='profile'),
    path(
        '<int:article_id>/',
        views.article_view, name='article_view'
    )
]
