from django.core.paginator import Paginator
from django.shortcuts import get_object_or_404, redirect, render

from .forms import ArticleForm
from .models import Article, Author


def index(request):
    articles = Article.objects.all()
    paginator = Paginator(articles, 10)
    page_number = request.GET.get('page')
    page = paginator.get_page(page_number)
    return render(
        request, 'index.html', {'paginator': paginator, 'page': page}
    )


def author_index(request):
    authors = Author.objects.all()
    return render(
        request, 'author_index.html', {'authors': authors}
    )


def article_view(request, article_id):
    article = get_object_or_404(
        Article, id=article_id
    )
    return render(
        request, 'article_view.html', {'article': article}
    )


def profile(request, author_id):
    author = get_object_or_404(Author, id=author_id)
    articles = author.articles.all()
    paginator = Paginator(articles, 10)
    page_number = request.GET.get('page')
    page = paginator.get_page(page_number)
    return render(request, 'profile.html', {
            'author': author,
            'page': page,
            'paginator': paginator
    })


def new_article(request):
    form = ArticleForm(request.POST or None, files=request.FILES or None)
    if not form.is_valid():
        return render(request, 'new_article.html', {'form': form})
    form.save()
    return redirect('index')
