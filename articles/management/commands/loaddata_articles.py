import random

from django.core.management.base import BaseCommand
from faker import Faker

from articles.models import Article, Author


class Command(BaseCommand):
    help = 'Load data of articles'

    def handle(self, *args, **options):
        fake = Faker()
        for _ in range(30):
            title = fake.text(max_nb_chars=20)
            text = fake.text(max_nb_chars=1000)
            authors = [
                Author.objects.get(pk=random.randint(1, 30))
                for _ in range(random.randint(1, 3))
            ]
            article = Article.objects.create(title=title, text=text)
            for author in authors:
                article.author.add(author)
