import random

from django.core.management.base import BaseCommand
from faker import Faker

from articles.models import Author


class Command(BaseCommand):
    help = 'Load data of users'

    def handle(self, *args, **options):
        fake = Faker()
        for i in range(30):
            first_name, last_name = fake.first_name(), fake.last_name()
            Author.objects.get_or_create(
                first_name=first_name, last_name=last_name, position=i
            )
