import re

import requests
from bs4 import BeautifulSoup
from django.core.management.base import BaseCommand

from articles.models import Article, Author


def get_links_to_articles():
    """Запрос на получение списка ссылок на статьи"""
    response = requests.get('https://rb.ru/news/')
    urls = ('href="/(?:[a-zA-Z]|[0-9]'
            '|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
            )
    # Получаем и форматируем ссылки
    result = [
        'https://rb.ru/' + url[7:] for url in re.findall(urls, response.text)
        if url.rstrip().startswith('href="/news/') and len(url) > 12
    ]
    # Избавляемся от дубликатов
    result = list(dict.fromkeys(result))
    # Сокращаем список до 30 ссылок
    while len(result) > 30:
        result.pop()
    # Распологаем статьи в хронологическом порядке
    result.reverse()
    return result


def get_data(response):
    """Получить данные об авторе и статьях"""
    title = response.find(
        'div', {'itemprop': 'headline', 'style': 'display: none;'}
    ).text
    author = response.find('span', {'itemprop': 'name'}).text
    first_name, last_name = author.split()
    text = response.find(
        'div', {'class': 'article__content-block abv'}
    ).text
    data = {
        'title': title, 'text': text,
        'first_name': first_name, 'last_name': last_name
    }
    return data


class Command(BaseCommand):
    help = 'Load data of articles from RB.RU'

    def handle(self, *args, **options):
        result = get_links_to_articles()
        for i in range(len(result)):
            # Деалем запрос данных по ссылке на статью
            response = requests.get(result[i])
            soup = BeautifulSoup(response.text, 'lxml')
            # Получаем необходимые данные в словаре
            data = get_data(soup)
            print(f'{data.get("first_name")} {data.get("last_name")}. '
                  f'{data.get("title")}')
            # Проверяем, существует ли такой же объект Author
            author = Author.objects.filter(
                first_name=data.get("first_name"),
                last_name=data.get("last_name")
            ).exists()
            if not author:
                # Если авторов еще не существует, позиция нулевая
                position = 0
                if Author.objects.exists():
                    position = Author.objects.last().position + 1
                Author.objects.create(
                    first_name=data.get("first_name"),
                    last_name=data.get("last_name"), position=position
                )
            article = Article.objects.create(
                title=data.get('title'), text=data.get('text')
            )
            article.author.add(Author.objects.get(
                first_name=data.get("first_name"),
                last_name=data.get("last_name")
            ))
