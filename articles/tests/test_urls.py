from django.test import Client, TestCase
from django.urls import reverse

from articles.models import Article, Author


class StaticURLTests(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.author = Author.objects.create(
            first_name='User', last_name='Test', position=0
        )
        Article.objects.create(
            title='Какой-то заголовок', text='Текст'
        )
        cls.article = Article.objects.first()
        cls.article.author.add(cls.author)

    def setUp(self):
        self.guest_client = Client()

    def test_url_for_all_users(self):
        """Страницы доступны любому пользователю."""
        url_status_code = [
            reverse('index'),
            reverse('new_article'),
            reverse('author_index'),
            reverse('profile', kwargs={'author_id': 1}),
            reverse('article_view', kwargs={'article_id': 1}),
        ]
        for url in url_status_code:
            with self.subTest():
                response = self.guest_client.get(url)
                self.assertEqual(response.status_code, 200)

    def test_url_uses_correct_template(self):
        """URL-адрес использует соответствующий шаблон."""
        url_uses_templates = {
            reverse('index'): 'index.html',
            reverse('new_article'): 'new_article.html',
            reverse('profile', kwargs={'author_id': 1}): 'profile.html',
            reverse(
                'article_view', kwargs={'article_id': 1}
            ): 'article_view.html',
            reverse('author_index'): 'author_index.html',
        }
        for url, template in url_uses_templates.items():
            with self.subTest():
                response = self.guest_client.get(url)
                self.assertTemplateUsed(response, template)
