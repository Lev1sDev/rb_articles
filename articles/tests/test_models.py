from django.test import TestCase

from articles.models import Article, Author


class ArticleModelTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.author = Author.objects.create(
            first_name='User', last_name='Test', position=0
        )
        Article.objects.create(
            title='Какой-то заголовок', text='Текст'
        )
        cls.article = Article.objects.first()
        cls.article.author.add(cls.author)

    def test_verbose_name(self):
        """verbose_name в полях модели совпадает с ожидаемым."""
        article = ArticleModelTest.article
        field_verboses = {
            'text': 'Текст статьи',
            'title': 'Заголовок',
            'author': 'Автор',
        }
        for value, expected in field_verboses.items():
            with self.subTest(value=value):
                self.assertEqual(
                    article._meta.get_field(value).verbose_name, expected
                )

    def test_help_text(self):
        """help_text в полях модели совпадает с ожидаемым."""
        article = ArticleModelTest.article
        text = 'Удерживайте “Control“ (или “Command“ на Mac), чтобы ' \
               'выбрать несколько значений '
        self.assertEqual(article._meta.get_field('author').help_text, text)

    def test_object_name_is_text_field(self):
        """
        В поле __str__  объекта post записано значение поля post.text
        с ограничением в 15 символов.
        """
        article = ArticleModelTest.article
        expected_object_name = article.title[:15]
        self.assertEqual(expected_object_name, str(article))


class AuthorModelTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.author = Author.objects.create(
            first_name='User', last_name='Test', position=0
        )

    def test_verbose_name(self):
        """verbose_name в полях модели совпадает с ожидаемым."""
        author = AuthorModelTest.author
        field_verboses = {
            'first_name': 'Имя',
            'last_name': 'Фамилия',
            'position': 'Позиция',
        }
        for value, expected in field_verboses.items():
            with self.subTest(value=value):
                self.assertEqual(
                    author._meta.get_field(value).verbose_name, expected
                )

    def test_object_name_is_text_field(self):
        """
        В поле __str__ объекта модели записаны значения поля author.first_name
        и author.last_name .
        """
        author = AuthorModelTest.author
        expected_object_name = f'{author.first_name} {author.last_name}'
        self.assertEqual(expected_object_name, str(author))
