from django.test import Client, TestCase
from django.urls import reverse

from articles.models import Article, Author


class ArticlesPagesTests(TestCase):
    def setUp(self):
        self.guest_client = Client()

    def test_create_article(self):
        article_count = Article.objects.count()
        author1 = Author.objects.create(
            first_name='User1', last_name='Test1', position=0
        )
        author2 = Author.objects.create(
            first_name='User2', last_name='Test2', position=1
        )
        form_data = {
            'title': 'Заголовок',
            'text': 'Текст статьи',
            'author': [author1.id, author2.id]
        }
        response = self.guest_client.post(
            reverse('new_article'),
            data=form_data,
            follow=True
        )
        self.assertRedirects(response, reverse('index'))
        self.assertEqual(Article.objects.count(), article_count + 1)
