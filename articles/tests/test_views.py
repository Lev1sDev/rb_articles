from django import forms
from django.test import Client, TestCase
from django.urls import reverse

from articles.models import Article, Author


class ArticlesPagesTests(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.author = Author.objects.create(
            first_name='User', last_name='Test', position=0
        )
        Article.objects.create(
            title='Какой-то заголовок', text='Текст'
        )
        cls.article = Article.objects.first()
        cls.article.author.add(cls.author)

    def setUp(self):
        self.guest_client = Client()

    def test_show_correct_context(self):
        """
        В шаблон index, profile переданы правильные значения
        page и paginator из контекста
        """
        html_used_context = (
            reverse('index'),
            reverse('profile', kwargs={'author_id': 1}),
        )
        for html in html_used_context:
            with self.subTest():
                response = self.guest_client.get(html)
                self.assertEqual(response.context.get('page')[0].text, 'Текст')
                self.assertEqual(
                    response.context.get('page')[0].id, 1
                )
                for author in response.context.get('page')[0].author.all():
                    self.assertEqual(
                        author.first_name, 'User'
                    )
                    self.assertEqual(
                        author.last_name, 'Test'
                    )
                    self.assertEqual(
                        author.position, 0
                    )
                self.assertEqual(response.context.get('paginator').count, 1)
                self.assertEqual(
                    response.context.get('page').has_next(), False
                )

    def test_profile_show_correct_context(self):
        """Шаблон profile сформирован с правильным контекстом."""
        response = self.guest_client.get(
            reverse('profile', kwargs={'author_id': 1})
        )
        self.assertEqual(response.context.get('author').first_name, 'User')
        self.assertEqual(response.context.get('author').last_name, 'Test')
        self.assertEqual(response.context.get('author').position, 0)

    def test_article_show_correct_context(self):
        """Шаблон article_view сформирован с правильным контекстом."""
        response = self.guest_client.get(
            reverse('article_view', kwargs={'article_id': 1})
        )
        for author in response.context.get('article').author.all():
            self.assertEqual(
                author.first_name, 'User'
            )
            self.assertEqual(
                author.last_name, 'Test'
            )
            self.assertEqual(
                author.position, 0
            )
        self.assertEqual(response.context.get('article').id, 1)
        self.assertEqual(response.context.get('article').text, 'Текст')
        self.assertEqual(
            response.context.get('article').title, 'Какой-то заголовок'
        )

    def test_new_article_show_correct_context(self):
        """Шаблон new_article сформирован с правильным контекстом."""
        response = self.guest_client.get(reverse('new_article'))
        form_fields = {
            'text': forms.fields.CharField,
            'author': forms.fields.ChoiceField,
            'title': forms.fields.CharField,
        }
        for value, expected in form_fields.items():
            with self.subTest(value=value):
                form_field = response.context.get('form').fields.get(value)
                self.assertIsInstance(form_field, expected)

    def test_author_index_show_correct_context(self):
        """Шаблон author_index сформирован с правильным контекстом."""
        response = self.guest_client.get(reverse('author_index'))
        self.assertEqual(response.context.get('authors')[0].first_name, 'User')
        self.assertEqual(response.context.get('authors')[0].last_name, 'Test')
        self.assertEqual(response.context.get('authors')[0].position, 0)
