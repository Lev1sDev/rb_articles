# Generated by Django 3.2.3 on 2021-06-02 14:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0008_remove_author_sort'),
    ]

    operations = [
        migrations.AlterField(
            model_name='author',
            name='position',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Позиция'),
        ),
    ]
