# Generated by Django 3.2.3 on 2021-06-01 11:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0003_alter_article_options'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='author',
            options={'ordering': ['-pk'], 'verbose_name': 'Автор', 'verbose_name_plural': 'Авторы'},
        ),
    ]
