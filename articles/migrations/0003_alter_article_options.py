# Generated by Django 3.2.3 on 2021-06-01 11:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0002_alter_article_author'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='article',
            options={'ordering': ['-pk'], 'verbose_name': 'Статья', 'verbose_name_plural': 'Статьи'},
        ),
    ]
