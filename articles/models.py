from django.db import models


class Author(models.Model):
    first_name = models.CharField(max_length=20, verbose_name='Имя')
    last_name = models.CharField(max_length=20, verbose_name='Фамилия')
    position = models.PositiveSmallIntegerField(
        verbose_name='Позиция', null=True, blank=True
    )

    class Meta:
        ordering = ['-position']
        verbose_name = 'Автор'
        verbose_name_plural = 'Авторы'

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Article(models.Model):
    author = models.ManyToManyField(
        Author, verbose_name='Автор', related_name='articles',
        help_text='Удерживайте “Control“ (или “Command“ на Mac), чтобы '
                  'выбрать несколько значений '
    )
    title = models.CharField(max_length=60, verbose_name='Заголовок')
    text = models.TextField(verbose_name='Текст статьи')

    class Meta:
        ordering = ['-pk']
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'

    def __str__(self):
        return self.title[:15]
